package com.guoyansoft.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
@Configuration
public class DateConverter implements Converter<String, Date> {
    @Override
    public Date convert(String source) {
        SimpleDateFormat sim=null;
      if (source==""){
          return null;
      }else {
          if (source==null||(!source.contains("-")&&!source.contains("/"))){
              throw  new RuntimeException("日期格式有误!");
          }

          if (source.contains("-")){
              sim=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
          }

          if (source.contains("/")){
              sim=new SimpleDateFormat("yyyy/MM/dd HH/mm/ss");
          }
          Date date=null;
          try {
              date= sim.parse(source);
          } catch (ParseException e) {
              e.printStackTrace();
          }
          return date;
      }
      }
    }
