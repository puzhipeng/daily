package com.guoyansoft.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.ConversionService;

import java.util.HashSet;

public class DateConverterConfig {
     @Autowired
     private  DateConverter dateConverter;
      public ConversionService conversionService(){
          HashSet<Object> objects = new HashSet<>();
           objects.add(dateConverter);
          ConversionServiceFactoryBean bean=new ConversionServiceFactoryBean();
          bean.setConverters(objects);
         return bean.getObject();

    }
}
