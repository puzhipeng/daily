package com.guoyansoft.controller;

import com.guoyansoft.model.pojo.Doctor;
import com.guoyansoft.model.pojo.Tasklist;
import com.guoyansoft.model.pojo.User;
import com.guoyansoft.model.service.TasklistService;
import com.guoyansoft.model.service.UserService;
import com.guoyansoft.model.utils.LayuiData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@RestController
//@RequestMapping("/user")
public class UserController {
    @Autowired
     private UserService userService;
    @Autowired
    private TasklistService tasklistService;

      @RequestMapping("/select")
     public User select(String userName, String userPassword, HttpServletRequest request){
          HttpSession session = request.getSession();
           User user=new User();
           user.setU_name(userName);
            user.setU_password(userPassword);
          User select = userService.select(user);
           session.setAttribute("user",select);
           return select;

      }

    @RequestMapping("/getUser")
    public User getUserName(HttpServletRequest request) {

        HttpSession session = request.getSession();
        User success = (User) session.getAttribute("user");
        return success;

    }
    @RequestMapping("/out")
    public void out(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        session.invalidate();
        try {
            request.getRequestDispatcher("/log.html").forward(request,response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/getUid")

     public Doctor getUid(Integer u_id){
        Doctor doctor = userService.selectByid(u_id);

           return  doctor;
    }

}
