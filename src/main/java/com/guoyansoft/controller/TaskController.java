package com.guoyansoft.controller;

import com.guoyansoft.model.pojo.Task;
import com.guoyansoft.model.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController {
  @Autowired

     private TaskService taskService;

    @RequestMapping("/insert")
     public  String insert(Integer patient, Integer doctor, Integer administrative){
        Task task=new Task();
        String s=null;
        task.setT_patient(patient);
        task.setDo_id(doctor);
        task.setAd_id(administrative);
         task.setDate(new Date());
        Calendar ac=Calendar.getInstance();
        int day = ac.get(Calendar.DATE);
        ac.set(Calendar.DATE, day+1);
         task.setExpirationTime(ac.getTime());
        if (doctor!=null&&administrative!=null){
             s="1";
            task.setState(s);
        }else {
            s="0";
            task.setState(s);
        }
        return  taskService.insert(task);
    }


//领取任务
    @RequestMapping("/update")
    public  String update(Integer t_id, Integer do_id,Integer ad_id,String state ){

        Task task=new Task();
        task.setT_id(t_id);
        task.setDo_id(do_id);
        task.setAd_id(ad_id);
        task.setState(state);
        return  taskService.update(task);
    }


//    移交任务

    @RequestMapping("/transfer")
    public  String transfer(Integer t_id, Integer d_id,Integer a_id){
        Task task=new Task();
        task.setT_id(t_id);
        task.setDo_id(d_id);
        task.setAd_id(a_id);
        return  taskService.transfer(task);
    }


//    修改状态
    @RequestMapping("/updateState")
    public  String updateState(Integer t_id,String state ){

        Task task=new Task();
        task.setT_id(t_id);
        task.setState(state);
        return  taskService.updateState(task);
    }

    @RequestMapping("/delete")
    public  String delete(Integer t_id){
      return   taskService.delete(t_id);
    }

    @RequestMapping("/select")
    public List<Task> select(){
        return taskService.selectState();
    }
}
