package com.guoyansoft.controller;
import com.guoyansoft.model.pojo.Pattern;
import com.guoyansoft.model.service.PatternService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/pattern")
public class PatternController {
    @Autowired
      private PatternService patternService;

      @RequestMapping("/select")

      public List<Pattern> select(){

          return   patternService.select();
      }
}
