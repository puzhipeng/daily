package com.guoyansoft.controller;

import com.guoyansoft.model.pojo.Type;
import com.guoyansoft.model.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/type")
public class TypeController {

    @Autowired
     private TypeService typeService;
    @RequestMapping("/select")
    public List<Type> select(){
       return   typeService.select();
     }
 }
