package com.guoyansoft.controller;

import com.guoyansoft.model.pojo.Repository;
import com.guoyansoft.model.service.RepositoryService;
import com.guoyansoft.model.utils.LayuiData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/repository")
public class RepositoryController {
    @Autowired
    private RepositoryService repositoryService;


    @RequestMapping("/selectBypage")
    public  LayuiData<Repository> selectBypage(Integer limit, Integer page,Integer k_type,Integer k_id){

        Map<String, Object> params = new HashMap();

        Integer pagSize = limit == null ? 10 : limit;
        Integer curPage = page == null ? 1 : page;

        curPage = curPage < 1 ? 1 : curPage;

        params.put("k_type",k_type);
        params.put("k_id",k_id);

        LayuiData<Repository> lad = null;
        try {
            lad = repositoryService.selectBypage(curPage, pagSize, params);

            return lad;
        } catch (SQLException e) {
            e.printStackTrace();
        }
   return null;
      }


    @RequestMapping("/select")

    public List<Repository> select(Integer k_id){
       return repositoryService.select(k_id);
    }
}
