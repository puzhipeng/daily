package com.guoyansoft.controller;

import com.guoyansoft.model.pojo.Administrative;
import com.guoyansoft.model.service.AdministrativeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/administrative")
public class AdministrativeController {

      @Autowired
    private AdministrativeService administrativeService;

      @RequestMapping("/select")

      public List<Administrative> select(){
         return    administrativeService.select();
      }
}
