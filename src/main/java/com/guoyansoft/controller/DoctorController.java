package com.guoyansoft.controller;

import com.guoyansoft.model.pojo.Doctor;
import com.guoyansoft.model.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/doctor")
public class DoctorController {
    @Autowired
      private DoctorService doctorService;
    @RequestMapping("/select")

     public List<Doctor> select(Integer a_id){

         return doctorService.select(a_id);
    }

}
