package com.guoyansoft.controller;

import com.guoyansoft.model.pojo.Knowledge;
import com.guoyansoft.model.service.KnowledgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/knowledge")
public class KnowledgeController {
    @Autowired
    private KnowledgeService knowledgeService;


    @RequestMapping("/select")
    public List<Knowledge> select(Integer k_type){
        Map<String,Object> map=new HashMap<>();
        map.put("k_type",k_type);
        return knowledgeService.select(map);

    }
}
