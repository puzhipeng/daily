package com.guoyansoft.controller;

import com.guoyansoft.model.pojo.Introduced;
import com.guoyansoft.model.service.IntroducedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/introduce")
public class IntroduceController {
    @Autowired
    private IntroducedService indus;

     @RequestMapping("/add")
    public String add(String name,Integer kid,String introduce){

         Introduced introduced=new Introduced();
         introduced.setName(name);
         introduced.setK_id(kid);
         introduced.setIntroduce(introduce);
         String insert = indus.insert(introduced);

     return insert;
     }


    @RequestMapping("/update")
    public  String update(String name,Integer kid,String introduce,Integer id){

        Introduced introduced=new Introduced();
        introduced.setId(id);
        introduced.setName(name);
        introduced.setK_id(kid);
        introduced.setIntroduce(introduce);
        String insert = indus.update(introduced);
        return  insert;
    }

    @RequestMapping("/delete")

    public  String delete(Integer id){
        String delete = indus.delete(id);
        return delete;

    }
}
