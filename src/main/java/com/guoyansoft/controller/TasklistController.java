package com.guoyansoft.controller;

import com.guoyansoft.model.pojo.Repository;
import com.guoyansoft.model.pojo.Tasklist;
import com.guoyansoft.model.service.TasklistService;
import com.guoyansoft.model.utils.LayuiData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/tasklist")
public class TasklistController {
    @Autowired
    private TasklistService tasklistService;

    @RequestMapping("/select")
    public LayuiData<Tasklist> select(Integer limit, Integer page,Integer p_id,Integer a_id,Integer d_id,String state){
        Map<String, Object> params = new HashMap();

        Integer pagSize = limit == null ? 10 : limit;
        Integer curPage = page == null ? 1 : page;

        curPage = curPage < 1 ? 1 : curPage;

        LayuiData<Tasklist> lad = null;
          params.put("p_id",p_id);
          params.put("a_id",a_id);
          params.put("d_id",d_id);
          params.put("state",state);
        try {
            lad = tasklistService.selectBypage(curPage, pagSize, params);

            return lad;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;


    }
}
