package com.guoyansoft.controller;

import com.guoyansoft.model.pojo.Doctor;
import com.guoyansoft.model.pojo.Patient;
import com.guoyansoft.model.service.DoctorService;
import com.guoyansoft.model.service.PatientService;
import com.guoyansoft.model.utils.LayuiData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.*;

@RestController
@RequestMapping("/patient")
public class PatientController {
    @Autowired
    private PatientService patientService;
    @RequestMapping("/select")

    public List<Patient> select(){

        return patientService.select();
    }
    @RequestMapping("/insert")

     public  String insert(String p_name, String p_card, String p_gender, String p_iphone, Integer a_id,  Date p_date, String p_result, String p_entity, String p_advice, Integer d_id,  String p_ptype, String p_diagnose){

           Patient patient=new Patient();
             patient.setP_name(p_name);
             patient.setP_card(p_card);
             patient.setP_gender(p_gender);
             patient.setP_iphone(p_iphone);
             patient.setA_id(a_id);
             patient.setP_date(p_date);
             patient.setP_result(p_result);
             patient.setP_entity(p_entity);
             patient.setP_advice(p_advice);
           patient.setD_id(d_id);
           patient.setP_ptype(p_ptype);
             patient.setP_diagnose(p_diagnose);
        String replace = UUID.randomUUID().toString().replace("-", "");
        StringBuilder sb=new StringBuilder();
        for (int i = 0; i <replace.length() ; i++) {
            char c = replace.charAt(i);
            if (c>=48&&c<=57){
                sb.append(c);
            }
        }

             patient.setP_medical(sb.substring(0,6));

        return  patientService.insert(patient);

    }

    @RequestMapping("/update")

    public  String update(Integer p_id,String p_name, String p_card, String p_gender, String p_iphone, Integer a_id,  Date p_date, String p_result, String p_entity, String p_advice, Integer d_id,  String p_ptype, String p_diagnose){
        String sd=null;
        Patient patient=new Patient();
        patient.setP_id(p_id);
        patient.setP_name(p_name);
        patient.setP_card(p_card);
        patient.setP_gender(p_gender);
        patient.setP_iphone(p_iphone);
        patient.setA_id(a_id);
        patient.setP_date(p_date);
        patient.setP_result(p_result);
        patient.setP_entity(p_entity);
        patient.setP_advice(p_advice);
        patient.setD_id(d_id);
        patient.setP_ptype(p_ptype);
        patient.setP_diagnose(p_diagnose);
        return  patientService.update(patient);
    }


    @RequestMapping("/delete")

     public  String delete(Integer p_id){
     return    patientService.delete(p_id);

    }
    @RequestMapping("/deleteBatch")

    public  String delete(Integer [] p_id){
        return    patientService.deleteBatch(p_id);

    }

    @RequestMapping("/selectBypage")

      public LayuiData<Patient>  selectBypage(Integer limit, Integer page,String p_medical,String p_name,String p_entity,String p_ptype,String administrative,String doctor){

        Map<String, Object> params = new HashMap();

        Integer pagSize = limit == null ? 3 : limit;
        Integer curPage = page == null ? 1 : page;

        curPage = curPage < 1 ? 1 : curPage;
          params.put("p_medical",p_medical);
          params.put("p_name",p_name==null?"":"%"+p_name+"%");
          params.put("p_entity",p_entity);
           if ("门诊患者".equals(p_ptype)){
               p_ptype="0";
               params.put("p_ptype",p_ptype);
           }else if ("住院患者".equals(p_ptype)){
               p_ptype="1";
               params.put("p_ptype",p_ptype);
           }else {
               p_ptype=null;
               params.put("p_ptype",p_ptype);
           }

          params.put("administrative",administrative);
          params.put("doctor",doctor);


        try {

            LayuiData<Patient> lad =patientService.selectBypage(curPage,pagSize,params);
            return lad;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;


    }



    }
