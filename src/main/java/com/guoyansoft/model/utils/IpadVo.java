package com.guoyansoft.model.utils;

import com.guoyansoft.model.pojo.Tasklist;

import java.util.List;

public class IpadVo {
     private List<Integer> ids;
     private  List<Tasklist> insertBatch;

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    public List<Tasklist> getInsertBatch() {
        return insertBatch;
    }

    public void setInsertBatch(List<Tasklist> insertBatch) {
        this.insertBatch = insertBatch;
    }
}
