package com.guoyansoft.model.utils;

import java.util.List;

public class LayuiData<T> {
     /*
      * @description:
      *
 {
  "code": 0,
  "msg": "",
  "count": 1000,
  "data": [{}, {}]
}
      * @author: puZhipeng
      * @date: 2022/7/6 17:50
      * @param:
      * @return:
      **/


    private Integer code = 0;
    private String msg = "";
    //      总记录数
    private Integer count;
    //      当前页数据
    private List<T> data;


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
