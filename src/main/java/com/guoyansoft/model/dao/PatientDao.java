package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Patient;
import com.guoyansoft.model.utils.IpadVo;
import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Mapper
public interface PatientDao {
//     任务表里面用到id和那么
     List<Patient> select();
      int insert(Patient patient);
      int update(Patient patient);
      int delete(Integer p_id);
      int deleteBatch(IpadVo ids);
    List<Patient> selectBypage(Map<String, Object> params) throws SQLException;
    Integer selectTotal(Map<String,Object> map);


}
