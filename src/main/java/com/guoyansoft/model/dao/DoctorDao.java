package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Doctor;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface DoctorDao {

       List<Doctor> select(Integer a_id);
}
