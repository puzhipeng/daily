package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Course;
import com.guoyansoft.model.pojo.Teacher;
import com.guoyansoft.model.utils.IpadVo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface CourseDao {
      List<Course> select();
      int insert(Course course);
      int update(Course course);
      int delete(Integer Id);
      int deleteBatch(IpadVo ids);
      List<Course> selectBypage(Map<String, Object> params) throws SQLException;
      Integer selectTotal(Map<String,Object> map);
}
