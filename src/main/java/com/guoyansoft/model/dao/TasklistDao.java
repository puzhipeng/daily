package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Task;
import com.guoyansoft.model.pojo.Tasklist;
import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
@Mapper
public interface TasklistDao {
    List<Tasklist> selectBypage(Map<String, Object> params) throws SQLException;
    Integer selectTotal(Map<String,Object> map);
}
