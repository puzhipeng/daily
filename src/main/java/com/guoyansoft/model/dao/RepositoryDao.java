package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Repository;
import com.guoyansoft.model.pojo.Task;
import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
@Mapper
public interface RepositoryDao {
    List<Repository> selectBypage(Map<String, Object> params) throws SQLException;
    Integer selectTotal(Map<String,Object> map);
    List<Repository> select(Integer k_id);
}
