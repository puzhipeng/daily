package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Knowledge;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface KnowledgeDao {

     List<Knowledge> select(Map<String,Object> map);
}
