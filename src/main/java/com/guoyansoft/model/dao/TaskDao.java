package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Task;
import com.guoyansoft.model.utils.IpadVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TaskDao {
    int insert(Task task);
    int delete(Integer id);
    int update(Task task);
    int updateState(Task task);
    List<Task> selectState();
    int insertBatch(IpadVo ipadVo);
    int  transfer(Task task);

}
