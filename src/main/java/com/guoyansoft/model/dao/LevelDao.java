package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Level;

import java.util.List;

public interface LevelDao {

       List<Level> select();
}
