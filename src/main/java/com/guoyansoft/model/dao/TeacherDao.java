package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Employee;
import com.guoyansoft.model.pojo.Teacher;
import com.guoyansoft.model.utils.IpadVo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface TeacherDao {

    //List<Teacher> select();
   // Employee  GetByEmployee();
    int insert(Teacher teacher);
    int update(Teacher teacher);
    int delete(Integer Id);
    int deleteBatch(IpadVo ids);
    List<Teacher> selectBypage(Map<String, Object> params) throws SQLException;
    Integer selectTotal(Map<String,Object> map);
}
