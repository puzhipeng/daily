package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Pattern;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface PatternDao {

      List<Pattern> select();
}
