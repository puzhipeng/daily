package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Type;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface TypeDao {
       List<Type> select();
}
