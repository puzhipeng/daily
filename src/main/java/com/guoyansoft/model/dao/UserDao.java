package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Doctor;
import com.guoyansoft.model.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface UserDao {

      User select(User user);
      Doctor selectByid(Integer u_id);
}
