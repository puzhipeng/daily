package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Student;
import com.guoyansoft.model.utils.IpadVo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface StudentDao {

    List<Student> select();
    int insert(Student student);
    int update(Student student);
    int delete(Integer Id);
    int deleteBatch(IpadVo ids);
    List<Student> selectBypage(Map<String, Object> params) throws SQLException;
    Integer selectTotal(Map<String,Object> map);
}
