package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Administrative;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface AdministrativeDao {
       List<Administrative> select();
}
