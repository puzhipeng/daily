package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.ClassRoom;
import com.guoyansoft.model.pojo.Teacher;
import com.guoyansoft.model.utils.IpadVo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface ClassRoomDao {

      List<ClassRoom> select();
      int insert(ClassRoom classRoom);
      int update(ClassRoom classRoom);
      int delete(Integer Id);
      int deleteBatch(IpadVo ids);
      List<ClassRoom> selectBypage(Map<String, Object> params) throws SQLException;
      Integer selectTotal(Map<String,Object> map);
}
