package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Employee;
import com.guoyansoft.model.pojo.Patient;
import com.guoyansoft.model.utils.IpadVo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface EmployeeDao {

      List<Employee> select();
      Employee  GetByEmployee();
      int insert(Employee employee);
     int update(Employee employee);
    int delete(Integer Id);

    int deleteBatch(IpadVo ids);
    List<Employee> selectBypage(Map<String, Object> params) throws SQLException;
    Integer selectTotal(Map<String,Object> map);
}
