package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Employee;
import com.guoyansoft.model.pojo.Score;
import com.guoyansoft.model.utils.IpadVo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface ScoreDao {
    List<Score> select();
    //Score  GetByScore();
    int insert(Score score);
    int update(Score score);
    int delete(Integer Id);

    int deleteBatch(IpadVo ids);
    List<Employee> selectBypage(Map<String, Object> params) throws SQLException;
    Integer selectTotal(Map<String,Object> map);
}
