package com.guoyansoft.model.dao;

import com.guoyansoft.model.pojo.Introduced;
import com.guoyansoft.model.pojo.Task;
import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Mapper
public interface IntroducedDao {

      int insert(Introduced introduced);
      int update(Introduced introduced);
      int delete(Integer id);
      List<Introduced> select();
      int count();

}
