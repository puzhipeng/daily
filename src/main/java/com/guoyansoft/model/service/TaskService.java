package com.guoyansoft.model.service;

import com.guoyansoft.model.pojo.Task;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface TaskService {

    String insert(Task task);
    String delete(Integer id);
    String update(Task task);
    List<Task> selectState();
     String updateState(Task task);
    String  transfer(Task task);

}
