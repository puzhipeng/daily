package com.guoyansoft.model.service;

import com.guoyansoft.model.dao.KnowledgeDao;
import com.guoyansoft.model.pojo.Knowledge;

import java.util.List;
import java.util.Map;

public interface KnowledgeService {
    List<Knowledge> select(Map<String,Object> map);
}
