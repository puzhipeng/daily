package com.guoyansoft.model.service;

import com.guoyansoft.model.pojo.Patient;
import com.guoyansoft.model.utils.IpadVo;
import com.guoyansoft.model.utils.LayuiData;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface PatientService {
    List<Patient> select();
    String insert(Patient patient);
    String update(Patient patient);
    String delete(Integer p_id);
    String deleteBatch(Integer [] ids);
    LayuiData<Patient> selectBypage(Integer curPage, Integer pageSize, Map<String, Object> params) throws SQLException;
    Integer selectTotal(Map<String,Object> map);

}
