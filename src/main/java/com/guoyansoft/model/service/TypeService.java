package com.guoyansoft.model.service;

import com.guoyansoft.model.pojo.Type;

import java.util.List;

public interface TypeService {
    List<Type> select();
}
