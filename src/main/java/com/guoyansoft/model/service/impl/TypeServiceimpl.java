package com.guoyansoft.model.service.impl;

import com.guoyansoft.model.dao.TypeDao;
import com.guoyansoft.model.pojo.Type;
import com.guoyansoft.model.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class TypeServiceimpl implements TypeService {
@Autowired
    private TypeDao typeDao;
    @Override
    public List<Type> select() {
        return typeDao.select();
    }
}
