package com.guoyansoft.model.service.impl;

import com.guoyansoft.model.dao.RepositoryDao;
import com.guoyansoft.model.pojo.Repository;
import com.guoyansoft.model.service.RepositoryService;
import com.guoyansoft.model.utils.LayuiData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
@Service
public class RepositoryServiceimpl implements RepositoryService {
    @Autowired
 private RepositoryDao repository;
    @Override
    public LayuiData<Repository> selectBypage(Integer curPage, Integer pageSize,Map<String, Object> params) throws SQLException {

        Integer total = repository.selectTotal(params);

        LayuiData<Repository> lad = new LayuiData();
        params.put("startIndexKey", (curPage - 1) * pageSize);
        params.put("pageSizeKey", pageSize);

        List<Repository> list = repository.selectBypage(params);

        lad.setCount(total);
        lad.setData(list);

        return lad;
    }

    @Override
    public List<Repository> select(Integer k_id) {

        return repository.select(k_id);
    }


}
