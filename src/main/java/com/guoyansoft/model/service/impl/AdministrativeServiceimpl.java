package com.guoyansoft.model.service.impl;

import com.guoyansoft.model.dao.AdministrativeDao;
import com.guoyansoft.model.pojo.Administrative;
import com.guoyansoft.model.service.AdministrativeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AdministrativeServiceimpl implements AdministrativeService {
    @Autowired
     private AdministrativeDao administrativeDao;
    @Override
    public List<Administrative> select() {
        return administrativeDao.select();
    }
}
