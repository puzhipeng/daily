package com.guoyansoft.model.service.impl;

import com.guoyansoft.model.dao.TaskDao;
import com.guoyansoft.model.pojo.Task;
import com.guoyansoft.model.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
@Service
public class TaskServiceimpl implements TaskService {
    @Autowired
    private TaskDao taskDao;


    @Override
    public String insert(Task task) {

        return taskDao.insert(task)+"";
    }

    @Override
    public String delete(Integer id) {

        return taskDao.delete(id)+"";
    }

    @Override
    public String update(Task task) {
        return taskDao.update(task)+"";
    }

    @Override
    public List<Task> selectState() {

        return taskDao.selectState();
    }

    @Override
    public String updateState(Task task) {
        return taskDao.updateState(task)+"";
    }

    @Override
    public String transfer(Task task) {
        return taskDao.transfer(task)+"";
    }

}


