package com.guoyansoft.model.service.impl;

import com.guoyansoft.model.dao.IntroducedDao;
import com.guoyansoft.model.pojo.Introduced;
import com.guoyansoft.model.service.IntroducedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IntroducedServiceimpl implements IntroducedService {
    @Autowired
    private IntroducedDao intd;
    @Override
    public String insert(Introduced introduced) {

        return intd.insert(introduced)+"";
    }

    @Override
    public String update(Introduced introduced) {
        return intd.update(introduced)+"";
    }

    @Override
    public String delete(Integer id) {
        return intd.delete(id)+"";
    }
}
