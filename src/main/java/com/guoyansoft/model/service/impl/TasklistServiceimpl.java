package com.guoyansoft.model.service.impl;

import com.guoyansoft.model.dao.TasklistDao;
import com.guoyansoft.model.pojo.Repository;
import com.guoyansoft.model.pojo.Tasklist;
import com.guoyansoft.model.service.TasklistService;
import com.guoyansoft.model.utils.LayuiData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
@Service
public class TasklistServiceimpl implements TasklistService {
    @Autowired
     private TasklistDao tasklistDao;
    @Override
    public LayuiData<Tasklist> selectBypage(Integer curPage, Integer pageSize, Map<String, Object> params) throws SQLException {
        Integer total = tasklistDao.selectTotal(params);

        LayuiData<Tasklist> lad = new LayuiData();
        params.put("startIndexKey", (curPage - 1) * pageSize);
        params.put("pageSizeKey", pageSize);

        List<Tasklist> list = tasklistDao.selectBypage(params);

        lad.setCount(total);
        lad.setData(list);

        return lad;

    }
}
