package com.guoyansoft.model.service.impl;

import com.guoyansoft.model.dao.UserDao;
import com.guoyansoft.model.pojo.Doctor;
import com.guoyansoft.model.pojo.User;
import com.guoyansoft.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceimpl implements UserService {
    @Autowired
       private UserDao userDao;
    @Override
    public User select(User user) {
        return userDao.select(user);
    }

    @Override
    public Doctor selectByid(Integer u_id) {
        return userDao.selectByid(u_id);
    }


}
