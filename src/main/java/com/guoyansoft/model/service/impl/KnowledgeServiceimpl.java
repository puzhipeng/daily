package com.guoyansoft.model.service.impl;

import com.guoyansoft.model.dao.KnowledgeDao;
import com.guoyansoft.model.pojo.Knowledge;
import com.guoyansoft.model.service.KnowledgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class KnowledgeServiceimpl implements KnowledgeService {
    @Autowired
    private  KnowledgeDao knowledgeDao;

    @Override
    public List<Knowledge> select(Map<String, Object> map) {

        return knowledgeDao.select(map);
    }
}
