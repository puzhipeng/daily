package com.guoyansoft.model.service.impl;

import com.guoyansoft.model.dao.PatientDao;
import com.guoyansoft.model.dao.PatternDao;
import com.guoyansoft.model.pojo.Pattern;
import com.guoyansoft.model.service.PatternService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PatternServiceimpl implements PatternService {
    @Autowired
      private PatternDao patternDao;
    @Override
    public List<Pattern> select() {
        return patternDao.select();
    }
}
