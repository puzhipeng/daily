package com.guoyansoft.model.service.impl;

import com.guoyansoft.model.dao.PatientDao;
import com.guoyansoft.model.pojo.Patient;
import com.guoyansoft.model.service.PatientService;
import com.guoyansoft.model.utils.IpadVo;
import com.guoyansoft.model.utils.LayuiData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class PatientServiceimpl implements PatientService {
    @Autowired
    private PatientDao patientDao;
    @Override
    public List<Patient> select() {
        return patientDao.select();
    }

    @Override
    public String insert(Patient patient) {
        return patientDao.insert(patient)+"";
    }

    @Override
    public String update(Patient patient) {
        return patientDao.update(patient)+"";
    }

    @Override
    public String delete(Integer p_id) {
        return patientDao.delete(p_id)+"";
    }

    @Override
    public String deleteBatch(Integer [] ids) {
        IpadVo ipadVo=new IpadVo();
        ipadVo.setIds(Arrays.asList(ids));
        return  patientDao.deleteBatch(ipadVo)+"";
    }

    @Override
    public  LayuiData<Patient> selectBypage(Integer curPage, Integer pageSize,Map<String, Object> params) throws SQLException {
        Integer total = patientDao.selectTotal(params);
        LayuiData<Patient> lad = new LayuiData();
        params.put("startIndexKey", (curPage - 1) * pageSize);
        params.put("pageSizeKey", pageSize);
        List<Patient> list = patientDao.selectBypage(params);
        lad.setCount(total);
        lad.setData(list);

        return lad;
    }

    @Override
    public Integer selectTotal(Map<String, Object> map) {
        return null;
    }
}
