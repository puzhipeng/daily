package com.guoyansoft.model.service.impl;

import com.guoyansoft.model.dao.DoctorDao;
import com.guoyansoft.model.pojo.Doctor;
import com.guoyansoft.model.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DoctorServiceimpl implements DoctorService {
    @Autowired
     private DoctorDao doctorDao;
    @Override
    public List<Doctor> select(Integer a_id) {
        return doctorDao.select(a_id);
    }
}
