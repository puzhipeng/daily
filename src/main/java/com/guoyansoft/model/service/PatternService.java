package com.guoyansoft.model.service;

import com.guoyansoft.model.pojo.Pattern;

import java.util.List;

public interface PatternService {
    List<Pattern> select();
}
