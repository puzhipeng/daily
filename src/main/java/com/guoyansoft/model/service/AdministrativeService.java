package com.guoyansoft.model.service;

import com.guoyansoft.model.pojo.Administrative;

import java.util.List;

public interface AdministrativeService {
    List<Administrative> select();
}
