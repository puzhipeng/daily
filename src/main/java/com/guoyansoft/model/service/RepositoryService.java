package com.guoyansoft.model.service;

import com.guoyansoft.model.pojo.Repository;
import com.guoyansoft.model.utils.LayuiData;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface RepositoryService {
    LayuiData<Repository> selectBypage(Integer curPage, Integer pageSize, Map<String, Object> params) throws SQLException;
    List<Repository> select(Integer k_id);
}
