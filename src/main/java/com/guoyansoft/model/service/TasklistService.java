package com.guoyansoft.model.service;

import com.guoyansoft.model.pojo.Repository;
import com.guoyansoft.model.pojo.Tasklist;
import com.guoyansoft.model.utils.LayuiData;

import java.sql.SQLException;
import java.util.Map;

public interface TasklistService {

    LayuiData<Tasklist> selectBypage(Integer curPage, Integer pageSize, Map<String, Object> params) throws SQLException;
}
