package com.guoyansoft.model.service;

import com.guoyansoft.model.pojo.Doctor;
import com.guoyansoft.model.pojo.User;

public interface UserService {
    User select(User user);
    Doctor selectByid(Integer u_id);
}
