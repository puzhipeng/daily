package com.guoyansoft.model.service;

import com.guoyansoft.model.pojo.Doctor;

import java.util.List;

public interface DoctorService {

    List<Doctor> select(Integer a_id);
}
