package com.guoyansoft.model.service;

import com.guoyansoft.model.dao.IntroducedDao;
import com.guoyansoft.model.pojo.Introduced;

import java.util.List;

public interface IntroducedService {
    String insert(Introduced introduced);
    String update(Introduced introduced);
    String delete(Integer id);
}
