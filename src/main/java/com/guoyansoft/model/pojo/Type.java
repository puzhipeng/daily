package com.guoyansoft.model.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Type {

     private  Integer Id;
     private  String typeName;
}
