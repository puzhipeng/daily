package com.guoyansoft.model.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
public class Pattern {
      private Integer  p_id;
      private String p_name;
}
