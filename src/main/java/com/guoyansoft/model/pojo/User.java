package com.guoyansoft.model.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class User {
       private  Integer u_id;
       private  String u_name;
       private String u_password;
       private Integer  u_level;
       private String  l_name;
}
