package com.guoyansoft.model.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.function.DoubleBinaryOperator;

public class Score implements Serializable {
      private  Integer Id;
      private  String name;
      private  String No;
      private Integer Age;
      private  String  Sex;
      private String  classRoom;
      private Integer courseId;
      private  String c_name;
      private Double  grade;
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
      private Date time;
      private  String title;

    public Score(Integer id, String name, String no, Integer age, String sex, String classRoom, Integer courseId, String c_name, Double grade, Date time, String title) {
        Id = id;
        this.name = name;
        No = no;
        Age = age;
        Sex = sex;
        this.classRoom = classRoom;
        this.courseId = courseId;
        this.c_name = c_name;
        this.grade = grade;
        this.time = time;
        this.title = title;
    }

    public Score() {
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNo() {
        return No;
    }

    public void setNo(String no) {
        No = no;
    }

    public Integer getAge() {
        return Age;
    }

    public void setAge(Integer age) {
        Age = age;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }
}
