package com.guoyansoft.model.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
public class Patient implements Serializable {
      private  Integer p_id;
      private  String p_name;
      private  String p_card;
      private  String p_gender;
      private  String p_iphone;
      private  Integer a_id;
      private  String  a_name;
      @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
      private Date p_date;
      private String p_result;
      private String p_entity;
      private String p_advice;
      private Integer d_id;
      private  String d_name;
      private  String  p_ptype;
      private  String  p_diagnose;
      private String p_medical;


      public Patient(Integer p_id, String p_name, String p_card, String p_gender, String p_iphone, Integer a_id, String a_name, Date p_date, String p_result, String p_entity, String p_advice, Integer d_id, String d_name, String p_ptype, String p_diagnose, String p_medical) {
            this.p_id = p_id;
            this.p_name = p_name;
            this.p_card = p_card;
            this.p_gender = p_gender;
            this.p_iphone = p_iphone;
            this.a_id = a_id;
            this.a_name = a_name;
            this.p_date = p_date;
            this.p_result = p_result;
            this.p_entity = p_entity;
            this.p_advice = p_advice;
            this.d_id = d_id;
            this.d_name = d_name;
            this.p_ptype = p_ptype;
            this.p_diagnose = p_diagnose;
            this.p_medical = p_medical;
      }

      public Patient() {
      }

      public Integer getP_id() {
            return p_id;
      }

      public void setP_id(Integer p_id) {
            this.p_id = p_id;
      }

      public String getP_name() {
            return p_name;
      }

      public void setP_name(String p_name) {
            this.p_name = p_name;
      }

      public String getP_card() {
            return p_card;
      }

      public void setP_card(String p_card) {
            this.p_card = p_card;
      }

      public String getP_gender() {
            return p_gender;
      }

      public void setP_gender(String p_gender) {

                 this.p_gender = p_gender;
      }

      public String getP_iphone() {
            return p_iphone;
      }

      public void setP_iphone(String p_iphone) {
            this.p_iphone = p_iphone;
      }


      public String getA_name() {
            return a_name;
      }

      public void setA_name(String a_name) {
            this.a_name = a_name;
      }

      public Date getP_date() {
            return p_date;
      }

      public void setP_date(Date p_date) {
            this.p_date = p_date;
      }

      public String getP_result() {
            return p_result;
      }

      public void setP_result(String p_result) {
            this.p_result = p_result;
      }

      public String getP_entity() {
            return p_entity;
      }

      public void setP_entity(String p_entity) {
            this.p_entity = p_entity;
      }

      public String getP_advice() {
            return p_advice;
      }

      public void setP_advice(String p_advice) {
            this.p_advice = p_advice;
      }

      public Integer getA_id() {
            return a_id;
      }

      public void setA_id(Integer a_id) {
            this.a_id = a_id;
      }

      public Integer getD_id() {
            return d_id;
      }

      public void setD_id(Integer d_id) {
            this.d_id = d_id;
      }

      public String getD_name() {
            return d_name;
      }

      public void setD_name(String d_name) {
            this.d_name = d_name;
      }

      public String getP_ptype() {

            return p_ptype;
      }

      public void setP_ptype(String p_ptype) {
          if ("0".equals(p_ptype)){
                this.p_ptype = "门诊患者";
            }else if ("1".equals(p_ptype)){
                this.p_ptype = "住院患者";
          }else  if ("门诊患者".equals(p_ptype)){
                this.p_ptype = "0";
          }else {
                this.p_ptype = "1";
          }
      }

      public String getP_diagnose() {
            return p_diagnose;
      }

      public void setP_diagnose(String p_diagnose) {
           if ("0".equals(p_diagnose)){
                 this.p_diagnose = "复诊";
           }else if ("1".equals(p_diagnose)){
                 this.p_diagnose = "初诊";
           }else if ("复诊".equals(p_diagnose)){
                 this.p_diagnose = "0";
           }else {
                 this.p_diagnose = "1";
           }
      }

      public String getP_medical() {
            return p_medical;
      }

      public void setP_medical(String p_medical) {
            this.p_medical = p_medical;
      }
}
