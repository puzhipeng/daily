package com.guoyansoft.model.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Repository {
    private  Integer id;
    private String k_name;
    private String name;
    private  Integer k_id;      //类别
    private  String introduce;

}
