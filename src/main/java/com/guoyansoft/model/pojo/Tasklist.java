package com.guoyansoft.model.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@NoArgsConstructor
public class Tasklist {
  private Integer t_id;
  private String p_medical;
  private String p_name;
  private String p_gender;
  private String p_iphone;
  private String p_result;
  private String p_entity;
  private String p_advice;
  private Integer p_id;
  private Integer d_id;
  private Integer a_id;
  private String d_name;
  private String a_name;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Date date;
  private String state;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Date expirationTime;

  public Tasklist(Integer t_id, String p_medical, String p_name, String p_gender, String p_iphone, String p_result, String p_entity, String p_advice, Integer p_id, Integer d_id, Integer a_id, String d_name, String a_name, Date date, String state, Date expirationTime) {
    this.t_id = t_id;
    this.p_medical = p_medical;
    this.p_name = p_name;
    this.p_gender = p_gender;
    this.p_iphone = p_iphone;
    this.p_result = p_result;
    this.p_entity = p_entity;
    this.p_advice = p_advice;
    this.p_id = p_id;
    this.d_id = d_id;
    this.a_id = a_id;
    this.d_name = d_name;
    this.a_name = a_name;
    this.date = date;
    this.state = state;
    this.expirationTime = expirationTime;
  }

  public Integer getT_id() {
    return t_id;
  }

  public void setT_id(Integer t_id) {
    this.t_id = t_id;
  }

  public String getP_medical() {
    return p_medical;
  }

  public void setP_medical(String p_medical) {
    this.p_medical = p_medical;
  }

  public String getP_name() {
    return p_name;
  }

  public void setP_name(String p_name) {
    this.p_name = p_name;
  }

  public String getP_gender() {
    return p_gender;
  }

  public void setP_gender(String p_gender) {
    this.p_gender = p_gender;
  }

  public String getP_iphone() {
    return p_iphone;
  }

  public void setP_iphone(String p_iphone) {
    this.p_iphone = p_iphone;
  }

  public String getP_result() {
    return p_result;
  }

  public void setP_result(String p_result) {
    this.p_result = p_result;
  }

  public String getP_entity() {
    return p_entity;
  }

  public void setP_entity(String p_entity) {
    this.p_entity = p_entity;
  }

  public String getP_advice() {
    return p_advice;
  }

  public void setP_advice(String p_advice) {
    this.p_advice = p_advice;
  }

  public String getD_name() {
    return d_name;
  }

  public void setD_name(String d_name) {
    this.d_name = d_name;
  }

  public String getA_name() {
    return a_name;
  }

  public void setA_name(String a_name) {
    this.a_name = a_name;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {

    if ("0".equals(state)) {
      this.state = "未领取";
    } else if ("1".equals(state)) {
      this.state = "已领取";
    } else if ("2".equals(state)) {
      this.state = "已完成";
    } else if ("3".equals(state)) {
      this.state = "未完成";
    } else {
      this.state = "进行中";
    }

  }

  public Integer getP_id() {
    return p_id;
  }

  public void setP_id(Integer p_id) {
    this.p_id = p_id;
  }

  public Integer getD_id() {
    return d_id;
  }

  public void setD_id(Integer d_id) {
    this.d_id = d_id;
  }

  public Integer getA_id() {
    return a_id;
  }

  public void setA_id(Integer a_id) {
    this.a_id = a_id;
  }

  public Date getExpirationTime() {
    return expirationTime;
  }

  public void setExpirationTime(Date expirationTime) {
    this.expirationTime = expirationTime;
  }
}


