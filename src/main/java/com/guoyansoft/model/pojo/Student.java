package com.guoyansoft.model.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

public class Student  implements Serializable {
    private  Integer Id;
    private  String No;
    private  String Name;
    private Integer Age;
    private  String  Sex;
    private  Integer classRoomId;
    private  Integer    majorId;
    private String  classRoom;
    private  String  m_name;
    private  String  region;
    private  String  iPhone;
    private  String note;
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date  createTime;
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private  Date updateTime;

    public Student() {
    }

    public Student(Integer id, String no, String name, Integer age, String sex, Integer classRoomId, Integer majorId, String classRoom, String m_name, String region, String iPhone, String note, Date createTime, Date updateTime) {
        Id = id;
        No = no;
        Name = name;
        Age = age;
        Sex = sex;
        this.classRoomId = classRoomId;
        this.majorId = majorId;
        this.classRoom = classRoom;
        this.m_name = m_name;
        this.region = region;
        this.iPhone = iPhone;
        this.note = note;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNo() {
        return No;
    }

    public void setNo(String no) {
        No = no;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Integer getAge() {
        return Age;
    }

    public void setAge(Integer age) {
        Age = age;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }



    public String getM_name() {
        return m_name;
    }

    public void setM_name(String m_name) {
        this.m_name = m_name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getiPhone() {
        return iPhone;
    }

    public void setiPhone(String iPhone) {
        this.iPhone = iPhone;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(Integer classRoomId) {
        this.classRoomId = classRoomId;
    }

    public Integer getMajorId() {
        return majorId;
    }

    public void setMajorId(Integer majorId) {
        this.majorId = majorId;
    }

    @Override
    public String toString() {
        return "Student{" +
                "Id=" + Id +
                ", No='" + No + '\'' +
                ", Name='" + Name + '\'' +
                ", Age=" + Age +
                ", Sex='" + Sex + '\'' +
                ", classRoom='" + classRoom + '\'' +
                ", m_name='" + m_name + '\'' +
                ", note='" + note + '\'' +
                ", region='" + region + '\'' +
                ", iPhone='" + iPhone + '\'' +
                '}';
    }
}
