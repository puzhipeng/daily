package com.guoyansoft.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Course {

      private  Integer Id;
      private String name;
      private  Integer teacherId;
      private String teacherName;
      private  Integer classRoomId;
      private  Integer classRoomName;


}
