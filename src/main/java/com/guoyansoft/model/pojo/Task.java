package com.guoyansoft.model.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

public class Task implements Serializable {
    private  Integer t_id;
    private  Integer t_patient;
    private  Integer do_id;
    private  Integer ad_id;
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date date;
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private  Date   expirationTime;
    private  String  type;
    private  String  state;
    private  String  template;

    public Task() {
    }

    public Task(Integer t_id, Integer t_patient, Integer do_id, Integer ad_id, Date date, Date expirationTime, String type, String state, String template) {
        this.t_id = t_id;
        this.t_patient = t_patient;
        this.do_id = do_id;
        this.ad_id = ad_id;
        this.date = date;
        this.expirationTime = expirationTime;
        this.type = type;
        this.state = state;
        this.template = template;
    }

    public Integer getT_id() {
        return t_id;
    }

    public void setT_id(Integer t_id) {
        this.t_id = t_id;
    }

    public Integer getT_patient() {
        return t_patient;
    }

    public void setT_patient(Integer t_patient) {
        this.t_patient = t_patient;
    }

    public Integer getDo_id() {
        return do_id;
    }

    public void setDo_id(Integer do_id) {
        this.do_id = do_id;
    }

    public Integer getAd_id() {
        return ad_id;
    }

    public void setAd_id(Integer ad_id) {
        this.ad_id = ad_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {

        this.state = state;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}
