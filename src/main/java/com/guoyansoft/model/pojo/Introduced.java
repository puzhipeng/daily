package com.guoyansoft.model.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Introduced {
      private Integer id;
      private String name;
      private Integer k_id;
      private  String introduce;
      private  String image;

}
