package com.guoyansoft.model.pojo;

public class Administrative {
      private  Integer a_id;
      private  String a_name;

    public Administrative() {
    }

    public Administrative(Integer a_id, String a_name) {
        this.a_id = a_id;
        this.a_name = a_name;
    }

    public Administrative(String a_name) {
        this.a_name = a_name;
    }

    public Integer getA_id() {
        return a_id;
    }

    public void setA_id(Integer a_id) {
        this.a_id = a_id;
    }

    public String getA_name() {
        return a_name;
    }

    public void setA_name(String a_name) {
        this.a_name = a_name;
    }
}
