package com.guoyansoft.model.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Knowledge {
    private  Integer k_id;
    private  String  k_name;
}
