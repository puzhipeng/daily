package com.guoyansoft.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassRoom<T> implements Serializable {
     private  Integer Id;
     private  Integer teacherId;
     private  Integer studentId;
     private  String classRoomName;
     private  String teacherName;
     private List<T>  StudentList;
}
