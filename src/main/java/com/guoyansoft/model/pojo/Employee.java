package com.guoyansoft.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee implements Serializable {

      private  Integer Id;
      private  String  Account;
      private  String Password;
      private   Integer levelId;
      private  String  l_name;
}
