package com.guoyansoft.model.pojo;

import java.io.Serializable;

public class Doctor implements Serializable {
    private  Integer d_id;
    private  String  d_name;
    private  Integer d_administrative;
    private  Administrative administrative;
     private   String a_name;
    public Doctor() {
    }

    public Doctor(Integer d_id, String d_name, Integer d_administrative, Administrative administrative) {
        this.d_id = d_id;
        this.d_name = d_name;
        this.d_administrative = d_administrative;
        this.administrative = administrative;
    }

    public Integer getD_id() {
        return d_id;
    }

    public void setD_id(Integer d_id) {
        this.d_id = d_id;
    }

    public String getD_name() {
        return d_name;
    }

    public void setD_name(String d_name) {
        this.d_name = d_name;
    }

    public Integer getD_administrative() {
        return d_administrative;
    }

    public void setD_administrative(Integer d_administrative) {
        this.d_administrative = d_administrative;
    }

    public Administrative getAdministrative() {
        return administrative;
    }

    public String getA_name() {
        return a_name;
    }

    public void setA_name(String a_name) {
        this.a_name = a_name;
    }

    public void setAdministrative(Administrative administrative) {
        this.administrative = administrative;
    }
}
