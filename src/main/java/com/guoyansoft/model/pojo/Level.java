package com.guoyansoft.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Level {

      private  Integer l_id;
      private  String l_name;
      private  Integer l_level;
}
