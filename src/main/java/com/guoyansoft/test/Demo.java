package com.guoyansoft.test;

import java.util.concurrent.*;

public class Demo {

    public static void main(String[] args) {

        ExecutorService executorService = new ThreadPoolExecutor(2,
                Runtime.getRuntime().availableProcessors(), 3,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(3),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.CallerRunsPolicy());
        //创建固定大小的线程
        ExecutorService executorService1 = Executors.newFixedThreadPool(5);
        //创建可伸缩的线程
        Executors.newCachedThreadPool();

        try {
            for (int i = 1; i <= 9; i++) {
                executorService.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + "" + "ok");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            executorService.shutdown();
        }
        //TODO
        //  System.out.println(Runtime.getRuntime().availableProcessors());
    }
}
